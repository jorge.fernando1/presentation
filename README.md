# Sample project

This just a sample project to show my programming skills.
Is a clone of a personal project, create for a friend.

Is a simple project, but I use some features that all project basically uses:

- Devise.
- Responders.
- Nested forms.
- Views helpers.

A running version on Heroku:
[link](https://jorge-sample-app.herokuapp.com/)
