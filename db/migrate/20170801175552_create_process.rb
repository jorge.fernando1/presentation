class CreateProcess < ActiveRecord::Migration[5.1]
  def change
    create_table :processes do |t|
      t.string :description
      t.date :start_date
      t.date :end_date
      t.references :author, polymorphic: true
      t.references :defendant, polymorphic: true
      t.timestamps
    end
  end
end
