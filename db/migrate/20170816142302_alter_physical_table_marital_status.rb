class AlterPhysicalTableMaritalStatus < ActiveRecord::Migration[5.1]
  def change
    change_column :physical_people, :marital_status, 'integer USING marital_status::integer'
  end
end
