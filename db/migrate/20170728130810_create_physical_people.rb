class CreatePhysicalPeople < ActiveRecord::Migration[5.1]
  def change
    create_table :physical_people do |t|
      t.string :name
      t.string :doc
      t.string :rdoc
      t.string :mothers_name
      t.string :marital_status
      t.string :profession
      t.date :born_at
      t.timestamps
    end
  end
end
