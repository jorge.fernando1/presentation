class CreatePendency < ActiveRecord::Migration[5.1]
  def change
    create_table :pendencies do |t|
      t.string :description
      t.boolean :done
      t.references :process, foreign_key: true
    end
  end
end
