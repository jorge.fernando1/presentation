class CreatePhone < ActiveRecord::Migration[5.1]
  def change
    create_table :phones do |t|
      t.string :number, limit: 11
      t.references :physical_person, foreign_key: true, null: true
      t.references :legal_person, foreign_key: true, null: true
    end
  end
end
