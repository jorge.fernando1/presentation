class CreatePhase < ActiveRecord::Migration[5.1]
  def change
    create_table :phases do |t|
      t.string :description
      t.text :resume
      t.date :phase_date
      t.references :process, foreign_key: true
      t.timestamps
    end
  end
end
