class CreateAddress < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :name
      t.string :number
      t.string :additional
      t.string :neighborhood
      t.string :city
      t.string :state
      t.references :physical_person
      t.references :legal_person
      t.timestamps
    end
  end
end
