class CreateLegalPeople < ActiveRecord::Migration[5.1]
  def change
    create_table :legal_people do |t|
      t.string :legal_name
      t.string :trading_name
      t.string :doc
      t.string :state_registration
      t.timestamps
    end
  end
end
