# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170817190428) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "name"
    t.string "number"
    t.string "additional"
    t.string "neighborhood"
    t.string "city"
    t.string "state"
    t.bigint "physical_person_id"
    t.bigint "legal_person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["legal_person_id"], name: "index_addresses_on_legal_person_id"
    t.index ["physical_person_id"], name: "index_addresses_on_physical_person_id"
  end

  create_table "legal_people", force: :cascade do |t|
    t.string "legal_name"
    t.string "trading_name"
    t.string "doc"
    t.string "state_registration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pendencies", force: :cascade do |t|
    t.string "description"
    t.boolean "done"
    t.bigint "process_id"
    t.index ["process_id"], name: "index_pendencies_on_process_id"
  end

  create_table "phases", force: :cascade do |t|
    t.string "description"
    t.text "resume"
    t.date "phase_date"
    t.bigint "process_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["process_id"], name: "index_phases_on_process_id"
  end

  create_table "phones", force: :cascade do |t|
    t.string "number", limit: 11
    t.bigint "physical_person_id"
    t.bigint "legal_person_id"
    t.index ["legal_person_id"], name: "index_phones_on_legal_person_id"
    t.index ["physical_person_id"], name: "index_phones_on_physical_person_id"
  end

  create_table "physical_people", force: :cascade do |t|
    t.string "name"
    t.string "doc"
    t.string "rdoc"
    t.string "mothers_name"
    t.integer "marital_status"
    t.string "profession"
    t.date "born_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "processes", force: :cascade do |t|
    t.string "description"
    t.date "start_date"
    t.date "end_date"
    t.string "author_type"
    t.bigint "author_id"
    t.string "defendant_type"
    t.bigint "defendant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_processes_on_author_type_and_author_id"
    t.index ["defendant_type", "defendant_id"], name: "index_processes_on_defendant_type_and_defendant_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "pendencies", "processes"
  add_foreign_key "phases", "processes"
  add_foreign_key "phones", "legal_people"
  add_foreign_key "phones", "physical_people"
end
