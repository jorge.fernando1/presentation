class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update]

  def new
    @person = type_person
  end

  def index
    collection
    respond_with @people
    @people
  end

  def show
  end

  def edit
  end

  def update
    @person.update type_person_params
    respond_with @person, location: -> { person_path(@person, type: @person.class.type) }
  end

  def create
    @person = type_person
    @person.update type_person_params

    respond_with @person, location: -> { person_path(@person, type: @person.class.type) }
  end

  private

  def collection
    @filters = filter_params
    @people = AppFilters::People.new(@filters).filter
    @people = @people.page(params[:page])
  end

  def type_person
    case params[:type]
    when 'physical' then return PhysicalPerson.new
    when 'legal' then  return LegalPerson.new
    end
    PhysicalPerson.new
  end

  def set_person
    case params[:type]
    when 'physical' then @person = PhysicalPerson.find(params[:id])
    when 'legal' then @person = LegalPerson.find(params[:id])
    end
  end

  def type_person_params
    case params[:type]
    when 'physical' then return physical_person_params
    when 'legal' then return legal_person_params
    else raise 'Can\'t Verify the type of person'
    end
  end

  def physical_person_params
    params.require(:physical_person)
          .permit(:name, :doc, :rdoc, :mothers_name,
                  :profession, :born_at, :marital_status,
                  addresses_attributes: [:id, :name, :number, :additional,
                                         :neighborhood, :city, :state,
                                         :_destroy],
                  phones_attributes: [:id, :number, :_destroy])
  end

  def legal_person_params
    params.require(:legal_person)
          .permit(:legal_name, :doc, :trading_name, :state_registration,
                  addresses_attributes: [:id, :name, :number, :additional,
                                         :neighborhood, :city, :state,
                                         :_destroy],
                  phones_attributes: [:id, :number, :_destroy])
  end

  def filter_params
    return { type: 'physical' } unless params[:filters].present?
    params.require(:filters).permit(:search, :type)
  end

  def flash_interpolation_options
    { resource_name: @person.name }
  end
end
