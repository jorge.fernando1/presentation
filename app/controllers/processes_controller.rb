class ProcessesController < ApplicationController
  before_action :load_people, only: [:new, :edit, :update, :create]
  before_action :set_process, only: [:show, :edit, :update]

  respond_to :html, :docx

  def index
    collection
    respond_with @processes
  end

  def new
    @process = McomProcess.new
    @process.phases.new
    find_and_set_author
  end

  def show
    # For some reason, respond_with it doesnt work
    # So use the old and ugly, respond_to
    respond_to do |format|
      format.docx do
        filename = "#{@process.author.name}-#{@process.description}.docx"
        render docx: 'show', filename: filename
      end
      format.html { render :show }
    end
  end

  def edit
  end

  def create
    @process = CreateProcess.create process_params
    respond_with @process, location: -> { process_path(@process) }
  end

  def update
    UpdateProcess.update(@process, process_params)
    respond_with @process, location: -> { process_path(@process) }
  end

  private

  def collection
    @filters = filter_params
    process_filter = AppFilters::Process.new(@filters)

    @processes = process_filter.filter
    @processes = @processes.page(params[:page])
  end

  def set_process
    @process = McomProcess.find(params[:id])
  end

  def load_people
    @physical = PhysicalPerson.all
    @legal = LegalPerson.all
    @people = @physical + @legal
  end

  def find_and_set_author
    return unless params[:author]
    id, type = params[:author].split('-')
    author = find_person(id, type)
    return unless author
    @process.author = author
  end

  def find_person(id, type)
    case type
    when 'physical' then PhysicalPerson.find(id)
    when 'legal' then LegalPerson.find(id)
    end
  end

  def process_params
    params.require(:mcom_process).permit(:description, :author_id, :defendant_id,
                                         :start_date, :end_date,
                                         phases_attributes: [:id, :description, :phase_date, :resume, :_destroy],
                                         pendencies_attributes: [:id, :description, :done, :_destroy])
  end

  def filter_params
    return {} unless params[:filters].present?
    params.require(:filters).permit(:description, :start_date, :end_date)
  end
end
