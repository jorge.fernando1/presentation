class PhasesController < ApplicationController
  before_action :set_process

  def new
    @phase = @process.phases.build
  end

  def create
    @phase = Phase.new phase_params
    @phase.process = @process
    @phase.save

    respond_with @phase, location: -> { process_path(@process) }
  end

  private

  def set_process
    @process = McomProcess.find params[:process_id]
  end

  def phase_params
    params.require(:phase).permit(:description, :phase_date, :resume)
  end
end
