class McomProcess < ApplicationRecord
  self.table_name= 'processes'

  belongs_to :author, polymorphic: true
  belongs_to :defendant, polymorphic: true

  has_many :phases, inverse_of: :process, foreign_key: 'process_id'
  has_many :pendencies, inverse_of: :process, foreign_key: 'process_id'

  accepts_nested_attributes_for :phases, allow_destroy: true
  accepts_nested_attributes_for :pendencies, allow_destroy: true

  after_initialize :set_start_date

  validates :description, :start_date, presence: true
  validate :start_date_is_before_end_date
  validate :author_and_defendant_is_the_same

  def self.icon
    :gavel
  end

  private

  def set_start_date
    return unless self.new_record?
    self.start_date = Date.today
  end

  def start_date_is_before_end_date
    return unless end_date
    errors.add(:start_date, :before_end_date) if start_date > end_date
  end

  def author_and_defendant_is_the_same
    errors.add(:author, :is_same_as_defendant) if author == defendant
  end
end
