class LegalPerson < ApplicationRecord
  has_many :phones
  has_many :addresses
  has_many :processes, as: :author, class_name: 'McomProcess'
  has_many :processes, as: :defendant, class_name: 'McomProcess'

  accepts_nested_attributes_for :phones, allow_destroy: true
  accepts_nested_attributes_for :addresses, allow_destroy: true

  validates :legal_name, :trading_name, :doc, :state_registration, presence: true
  validates :doc, length: { is: 14 }

  def name
    self.legal_name
  end

  def name=(value)
    self.legal_name= value
  end

  def doc=(value)
    self[:doc] = value ? value.gsub(/\D/, '') : value
  end

  def doc_with_mask
    str = self[:doc]
    return unless str
    "#{str[0..1]}.#{str[2..4]}.#{str[5..7]}/#{str[8..11]}-#{str[12..13]}"
  end

  def processes
    McomProcess.where('(author_type = :type AND author_id = :id) OR
                       (defendant_type = :type AND defendant_id = :id)',
                     { type: self.class.to_s, id: self.id })
  end

  def id_to_select
    "#{id}-#{self.class.type}"
  end

  def to_s
    self.legal_name
  end

  def self.icon
    :'building-o'
  end

  def self.type
    'legal'
  end
end
