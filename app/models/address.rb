class Address < ApplicationRecord
  belongs_to :legal_person, optional: true
  belongs_to :physical_person, optional: true

  validates :name, :number, :neighborhood, :city, :state, presence: true
  validates :state, inclusion: { in: Mcom::Constants::STATES }

  def address
    "#{name}, #{number} - #{neighborhood}"
  end

  def full_address
    "#{address} - #{city}/#{state}"
  end

  def self.icon
    :home
  end
end
