class Phone < ApplicationRecord
  belongs_to :physical_person, optional: true
  belongs_to :legal_person, optional: true

  validates :number, presence: true, length: { in: 10..11 }

  def number=(value)
    self[:number] = value ? value.gsub(/\D/, '') : value
  end

  def number_with_mask
    value = self[:number]
    return if value.nil? || value.blank?

    ddd = value[0..1]
    prefix = value.size == 10 ? value[2..5] : value[2..6]
    suffix = value[-4..-1]
    "(#{ddd}) #{prefix}-#{suffix}"
  end

  def self.icon
    :phone
  end
end
