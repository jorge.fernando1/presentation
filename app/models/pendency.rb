class Pendency < ApplicationRecord
  belongs_to :process, autosave: true, class_name: 'McomProcess',
             foreign_key: 'process_id'

  validates :description, presence: true

  def self.icon
    :'check-square-o'
  end
end
