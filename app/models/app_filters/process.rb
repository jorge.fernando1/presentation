class AppFilters::Process
  def initialize(params)
    @params = params
    @results = McomProcess.all
  end

  def filter
    find_by_description
    find_by_dates
    @results
  end

  private

  # Create params getters
  [:description, :start_date, :end_date].each do |key|
    define_method key do
      @params[key]
    end
  end

  def find_by_description
    return unless description.present?
    @results = @results.where('description ILIKE ?', "%#{description}%")
  end

  def find_by_dates
    return unless start_date.present?
    @params[:end_date] = Date.today.strftime('%d/%m/%Y') unless end_date.present?

    @results = @results.where(start_date: start_date..end_date)
  end
end
