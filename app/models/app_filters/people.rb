class AppFilters::People
  def initialize(params)
    @params = params
  end

  def filter
    filter_by_type
    filter_by_search_term
    @people
  end

  private

  [:search, :type].each do |key|
    define_method key do
      @params[key]
    end
  end


  def filter_by_type
    case type
    when 'physical'
      @people = PhysicalPerson.all
    when 'legal'
      @people = LegalPerson.all
    else
      @people = PhysicalPerson.all
    end
  end

  def filter_by_search_term
    return unless search.present?
    search =~ /\d/ ? doc_search : name_search
  end

  def doc_search
    doc = search.gsub(/\D/, '')
    @people = @people.where('doc LIKE ?', "%#{doc}%")
  end

  def name_search
    field = type == 'physical' ? 'name' : 'trading_name'
    @people = @people.where("#{field} ILIKE ?", "%#{search}%")
  end
end
