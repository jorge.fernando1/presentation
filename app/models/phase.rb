class Phase < ApplicationRecord
  after_initialize :set_phase_date
  belongs_to :process, autosave: true, class_name: 'McomProcess', foreign_key: 'process_id'

  validates :description, :phase_date, :resume, presence: true

  def set_phase_date
    return unless self.new_record?
    self.phase_date = Date.today
  end

  def self.icon
    :'hourglass-3'
  end
end
