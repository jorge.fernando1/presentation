class PhysicalPerson < ApplicationRecord
  I18N_STATUS_BASE = 'activerecord.attributes.physical_person.marital_statuses'

  enum marital_status: { single: 0, married: 1, separate: 2,
                         divorced: 3, widowed: 4 }

  has_many :phones, inverse_of: :physical_person
  has_many :addresses, inverse_of: :physical_person
  has_many :processes, as: :author, class_name: 'McomProcess'
  has_many :processes, as: :defendant, class_name: 'McomProcess'

  accepts_nested_attributes_for :phones, allow_destroy: true
  accepts_nested_attributes_for :addresses, allow_destroy: true

  validates :name, :doc, :rdoc, :mothers_name, :profession,
            :born_at, :marital_status, presence: true

  validates :doc, length: { is: 11 }

  def id_to_select
    "#{id}-#{self.class.type}"
  end

  def doc=(value)
    self[:doc] = value ? value.gsub(/\D/, '') : value
  end

  def doc_with_mask
    str = self[:doc]
    return if str.nil? || str.blank?
    "#{str[0..2]}.#{str[3..5]}.#{str[6..8]}-#{str[9..10]}"
  end

  def processes
    McomProcess.where('(author_type = :type AND author_id = :id) OR
                       (defendant_type = :type AND defendant_id = :id)',
                     { type: self.class.to_s, id: self.id })
  end

  def marital_status_i18n
    I18n.t("#{I18N_STATUS_BASE}.#{self.marital_status}")
  end

  def to_s
    self.name
  end

  def self.marital_statuses_for_select
    marital_statuses.map do |status, _i|
      [I18n.t("#{I18N_STATUS_BASE}.#{status}"), status]
    end
  end

  def self.icon
    :user
  end

  def self.type
    'physical'
  end
end
