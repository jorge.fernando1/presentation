module ViewsHelper
  def name_for_model(model, type = :singular)
    count = type == :singular ? 1 : 2
    model.model_name.human(count: count)
  end

  def model_with_icon(model, type = :singular)
    fa_icon model.icon, text: name_for_model(model, type)
  end

  def action_title_for(object, action, options = {})
    count = action.eql?('index') ? 2 : 1
    resource_name = object.send(options[:attribute]) if options[:attribute]
    resource_name ||= object.model_name.human(count: count)

    text = find_text(object, action, resource_name)
    icon = fa_icon(options[:icon], text: text) if options[:icon]
    icon ? icon : text
  end

  def show_attr_info(model, attr, options = {})
    value = attr_value(model, attr, options)
    dd_klass = options.delete(:dd_class)
    dt = content_tag :dt, model.class.human_attribute_name(attr)
    dd = content_tag :dd, value, class: dd_klass
    dt + dd
  end

  def attr_value(model, attr, options = {})
    value = model.send(options[:method]) if options[:method].present?
    value ||= model.send(attr)
    return I18n.l(value, format: options[:format] || :default) if value.is_a?(Date)
    value
  end

  private

  def find_text(object, action_name, resource_name)
    klass_name = object.model_name.element
    text = I18n.t("action_title.#{klass_name}.#{action_name}", resource: resource_name, default: nil)
    text ||= I18n.t("action_title.#{action_name}", resource: resource_name)
    text
  end
end
