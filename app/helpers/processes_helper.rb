module ProcessesHelper
  def people_for_select(people)
    people.collect { |p| [p.name, p.id_to_select] }
  end
end
