# Create form_for fields
module FieldsHelper
  def simple_text(f, attr, options = {})
    field_base(f, attr, :text_field, options)
  end

  def simple_date(f, attr, options = {})
    value = f.object.send(attr)
    options[:value] = value ? I18n.l(value) : nil
    options[:class] << ' date' if options[:class]
    options[:class] ||= 'date'
    field_base(f, attr, :text_field, options)
  end

  def simple_area(f, attr, options = {})
    field_base(f, attr, :text_area, options)
  end

  def simple_password(f, attr, options = {})
    field_base(f, attr, :password_field, options)
  end

  def simple_checkbox(f, attr, options = {})
    klass = options[:class] || ''
    klass << ' checkbox'

    content_tag(:div, class: klass) do
      content_tag(:label) do
        f.check_box(attr) + f.label(attr, nil)
      end
    end
  end

  def simple_select(f, attr, options = {})
    label = f_label(f, attr)
    options[:class] << ' form-control' if options[:class]
    options[:class] ||= 'form-control'

    id = options.delete(:id)
    name = options.delete(:name)
    collection = options.delete(:collection)
    select = f.collection_select(attr, collection, id, name, {}, options)

    content_tag(:div, class: 'form-group') do
      label + select
    end
  end

  def simple_submit(f, name, options = {})
    options[:class] ||= ''
    options[:class] << ' btn btn-default'

    icon = options.delete(:icon)
    text = I18n.t(name) unless icon
    text ||= fa_icon icon, text: I18n.t(name)

    f.button(options) { text }
  end

  private


  def field_base(f, attr, type, options = {})
    options[:class] << ' form-control' if options[:class]
    options[:class] ||= 'form-control'
    label = f_label(f, attr)
    input = f.send(type, attr, options)
    content_tag(:div, class: 'form-group') do
      label + input
    end.html_safe
  end

  def f_label(f, attr)
    klass = 'control-label'
    f.label(attr, nil, class: klass)
  end
end
