module LinksHelper

  def button_with_icon(icon, text, url, options = {})
    options[:'data-container'] = 'body'

    klass = "tip btn #{ options[:style] || 'btn-default' }"
    options[:class] << klass if options[:class]
    options[:class] ||= klass
    icon = fa_icon(icon, text: text, right: options[:icon_right])
    link_to(url, options) { icon }
  end

  def link_with_icon(icon, text, url, options = {})
    options[:'data-container'] = 'body'
    icon = fa_icon(icon, text: text, right: options[:icon_right])
    link_to(url, options) { icon }
  end

  # Create Crud buttons
  Mcom::Constants::BUTTONS.each do |type, values|
    define_method "button_to_#{type}" do |url, opts = {}|
      opts[:'data-container'] = 'body'
      opts[:class].concat(' ').concat(values[:btn]) if opts[:class]
      opts[:class] ||= values[:btn]
      button_base(type, values[:icon], url, opts)
    end
  end

  # Create crud links
  Mcom::Constants::BUTTONS.each do |type, values|
    define_method "link_to_#{type}" do |url, options = {}|
      options[:'data-container'] = 'body'
      label = options.include?(:label) ? options[:label] : t("buttons.#{type}")
      options[:title] = label.nil? || label.empty? ? t("buttons.#{type}") : nil
      right_text = options[:right_icon].present? ? true : false
      icon = fa_icon(values[:icon], text: label, right: right_text)

      link_to(url, options) { icon }
    end
  end

  private

  def button_base(type, icon, url, opts = {})
    opts[:class] << ' btn tip'
    label = opts.include?(:label) ? opts[:label] : t("buttons.#{type}")
    opts[:title] = label.nil? || label.empty? ? t("buttons.#{type}") : nil
    right_text = opts[:right_icon].present? ? true : false

    icon = fa_icon(icon, text: label, right: right_text)

    link_to(url, opts) { icon }
  end
end
