module ApplicationHelper
  def msg_error(model)
    model_name = model.model_name.human
    count = model.errors.count
    error = I18n.t('words.error', count: model.errors.count)
    "#{model_name}: #{count} #{error}"
  end
end
