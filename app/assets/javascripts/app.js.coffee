$(document).on 'turbolinks:load', ->


  # Initialize the tooltips
  $('body').tooltip
    selector: '.tip'

  $('.date').mask('00/00/0000')
  $('.money-field').mask('#.##0,00', { reverse: true })
  $('.cpf').mask('000.000.000-00')
  $('.cnpj').mask('00.000.000/0000-00')

  PhoneMaskBehavior = (val) ->
    if val.replace(/\D/g, '').length == 11
      return '(00) 00000-0000'
    else
      return '(00) 0000-00009'

  PhoneMaskOptions = {
    onKeyPress: (val, e, field, options) ->
      field.mask(PhoneMaskBehavior.apply({}, arguments), options)
      return
  }
  $('.phone-text').mask(PhoneMaskBehavior, PhoneMaskOptions)

  # DocMaskBehavior = (val) ->
  #   if val.replace(/\D/g, '').length > 11
  #     return '00.000.000/0000-00'
  #   else
  #     return '000.000.000-000'
  #   return

  # DocMaskOptions = {
  #   onKeyPress: (val, e, field, options) ->
  #     field.mask(DocMaskBehavior.apply({}, arguments), options)
  #     return
  #   }

  # $('.doc-field').mask(DocMaskBehavior, DocMaskOptions)


  # Config datepicker
  $('.date').datepicker
    autoclose: true
    format: 'dd/mm/yyyy'
    language: 'pt-BR'
    todayHighlight: true
