class CreateProcess
  def self.create(params)
    self.new(params).create!
  end

  def initialize(params)
    @params = params || {}
    @process = McomProcess.new
  end

  def create!
    return @process if @params.empty?
    find_author_and_defendant!
    create_process!
    @process
  end

  private

  attr_accessor :params, :process

  def create_process!
    @process.assign_attributes @params
    @process.save
  end

  def find_author_and_defendant!
    @params[:author] = find_person @params.delete(:author_id)
    @params[:defendant] = find_person @params.delete(:defendant_id)
  end

  def find_person(id)
    id, type = id.split('-')
    case type
    when 'legal' then LegalPerson.find(id)
    when 'physical' then PhysicalPerson.find(id)
    end
  end
end
