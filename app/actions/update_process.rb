class UpdateProcess
  def self.update(process, params)
    self.new(process, params).update!
  end

  def initialize(process, params)
    @process = process
    @params = params
  end

  def update!
    find_author_and_defendant!
    update_process
    @process
  end

  private

  attr_accessor :process, :params

  def update_process
    @process.update @params
  end

  def find_author_and_defendant!
    @params[:author] = find_person @params.delete(:author_id)
    @params[:defendant] = find_person @params.delete(:defendant_id)
  end

  def find_person(id)
    id, type = id.split('-')
    case type
    when 'legal' then LegalPerson.find(id)
    when 'physical' then PhysicalPerson.find(id)
    end
  end
end
