require 'rails_helper'

describe CreateProcess do
  let(:author) { create(:physical_person) }
  let(:defendant) { create(:legal_person) }

  let(:author_id) { author.id_to_select }
  let(:defendant_id) { defendant.id_to_select }

  let(:process_params) do
    { description: 'processo', author_id: author_id,
      defendant_id: defendant_id, start_date: '02/08/2017' }
  end

  let(:pendencies_params) do
    { '0' => { description: 'Pendencia', done: '0' },
      '1' => { description: 'Pendencia', done: '0' }
    }
  end

  let(:phase_params) do
    { '0' => { description: 'Fase 1', phase_date: '02/08/2017',
               resume: 'Resumo' }
    }
  end

  describe '.create' do
    context 'process invalid' do
      it 'when params is nil' do
        process = CreateProcess.create(nil)
        expect(process).to be_invalid
      end

      it 'when params is empty' do
        process = CreateProcess.create({})
        expect(process).to be_invalid
      end

      it 'when some nested attribute is invalid' do
        phase_attrs = { description: '', phase_date: '02/08/2017', resume: 'ff' }
        params = process_params.merge(phases_attributes: { '0' => phase_attrs })
        process = CreateProcess.create(params)
        expect(process).to be_invalid
        expect(process.errors).to include :'phases.description'
      end
    end

    context 'Create the process' do
      it 'Without phases and pendencies' do
        process = CreateProcess.create(process_params)

        expect(process).to be_valid
        expect(process.author).to eq author
        expect(process.defendant).to eq defendant
        expect(process.start_date).to eq Date.parse('2017-08-02')
        expect(process.phases.count).to eq 0
        expect(process.pendencies.count).to eq 0
      end

      it 'Only with phases params' do
        params = process_params.merge(phases_attributes: phase_params)
        process = CreateProcess.create(params)
        phase = process.phases.first

        expect(process).to be_valid
        expect(process.author).to eq author
        expect(process.defendant).to eq defendant
        expect(process.start_date).to eq Date.parse('2017-08-02')
        expect(process.phases.count).to eq 1

        expect(phase.description).to eq 'Fase 1'
        expect(phase.phase_date).to eq Date.parse('2017-08-02')
        expect(phase.resume).to eq 'Resumo'
      end

      it 'Only with pendencies params' do
        params = process_params.merge(pendencies_attributes: pendencies_params)
        process = CreateProcess.create(params)

        expect(process).to be_valid
        expect(process.author).to eq author
        expect(process.defendant).to eq defendant
        expect(process.start_date).to eq Date.parse('2017-08-02')
        expect(process.pendencies.count).to eq 2

        process.pendencies.each do |pendency|
          expect(pendency.description).to eq 'Pendencia'
          expect(pendency.done).to eq false
        end
      end

      it 'with both phase and pendency' do
        params = process_params.merge(phases_attributes: phase_params,
                                      pendencies_attributes: pendencies_params)
        process = CreateProcess.create(params)

        expect(process).to be_valid
        expect(process.author).to eq author
        expect(process.defendant).to eq defendant
        expect(process.start_date).to eq Date.parse('2017-08-02')
        expect(process.phases.count).to eq 1
        expect(process.pendencies.count).to eq 2
      end
    end
  end
end
