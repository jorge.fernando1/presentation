FactoryGirl.define do
  factory :process, class: McomProcess do
    sequence(:description) { |n| "Process called number #{n}" }
    start_date { Date.today }
    author { create(:physical_person) }
    defendant { create(:legal_person) }

    after(:create) do |process|
      create(:phase, process: process)
      process
    end
  end
end
