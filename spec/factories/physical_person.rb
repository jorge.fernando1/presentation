FactoryGirl.define do
  factory :physical_person do
    sequence(:name) { |n| "Pessoa física número #{n}" }
    doc { '12345678910' }
    rdoc { 'RG: 123.456-7' }
    mothers_name { 'Nome mãe' }
    profession { 'Profissão' }
    born_at { 20.years.ago }
    marital_status { 'single' }
  end
end
