FactoryGirl.define do
  factory :legal_person do
    sequence(:legal_name) { |n| "Pessoa juridica #{n}" }
    sequence(:trading_name) { |n| "Empresa fantasia #{n}" }
    doc { '53652498000133' }
    state_registration { '123456789' }
  end
end
