FactoryGirl.define do
  factory :phase do
    description 'A little description of phase'
    phase_date { Date.today }
    resume 'A single text of resume'
  end
end
