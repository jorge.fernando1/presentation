source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby '2.3.4'

gem 'rails', '~> 5.1.2'
gem 'puma', '~> 3.7'
gem 'devise'
gem 'responders'
gem 'pg'
gem 'kaminari'
gem 'htmltoword', git: "https://github.com/karnov/htmltoword" # Easy way to create docx files
gem 'factory_girl_rails' # Create sample data easy


# HTML
gem 'haml'
gem 'haml-rails'
gem 'bootstrap-sass', '~> 3.3', '>= 3.3.6'
gem 'font-awesome-rails'


# CSS
gem 'sass-rails', '~> 5.0'

# JS
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jquery-rails'
gem "cocoon" # Made nested forms easy


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 3.5'
  gem 'database_cleaner'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
