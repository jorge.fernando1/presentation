# All I18n configs must be here

I18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

I18n.available_locales = [:pt_br]

I18n.default_locale = :pt_br
