module Mcom
  module Constants
    # buttons_to helper
    BUTTONS = {
      add: { icon: :plus, btn: 'btn-primary' },
      edit: { icon: :pencil, btn: 'btn-default' },
      show: { icon: :info, btn: 'btn-default' },
      back: { icon: 'arrow-left', btn: 'btn-default' },
      sign_in: { icon: 'sign-in', btn: 'btn-default' },
      sign_out: { icon: 'sign-out', btn: 'btn-default' }
    }.freeze

    FLASH_MESSAGES = {
      notice: { icon: 'info', body: 'success' },
      alert: { icon: 'times', body: 'danger' },
      error: { icon: 'times', body: 'danger' }
    }.freeze

    STATES_LABEL = [
      ['Acre', :ac], ['Alagoas', :al], ['Amapá', :ap], ['Amazonas', :am],
      ['Bahia', :ba], ['Ceará', :ce], ['Distrito Federal', :df],
      ['Espírito Santo', :es], ['Goiás', :go], ['Maranhão', :ma],
      ['Mato Grosso', :mt], ['Mato Grosso do Sul', :ms],
      ['Minas Gerais', :mg], ['Pará', :pa], ['Paraiba', :pb],
      ['Paraná', :pr], ['Pernambuco', :pe], ['Piauí', :pi],
      ['Rio de Janeiro', :rj], ['Rio Grande do Norte', :rn],
      ['Rio Grande do Sul', :rs], ['Rondônia', :ro], ['Roraima', :rr],
      ['Santa Catarina', :sc], ['São Paulo', :sp], ['Sergipe', :se],
      ['Totantins', :to]
    ].freeze

    STATES = ['ac', 'al', 'ap', 'am', 'ba', 'ce', 'df', 'es', 'go', 'ma',
              'mt', 'ms', 'mg', 'pa', 'pb', 'pr', 'pe', 'pi', 'rj', 'rn',
              'rs', 'ro', 'rr', 'sc', 'sp', 'se', 'to'].freeze
  end

end
