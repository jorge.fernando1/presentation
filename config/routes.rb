Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :people
  resources :processes do
    resources :phases, only: [:new, :create]
  end

  root to: 'home#index'
end
